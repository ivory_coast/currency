//
//  Transition.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import Foundation

protocol Transition: AnyObject {
    
    func perform()
}

protocol TransitionDelegate: AnyObject {
    
    func didFinish(_: Transition)
}
