//
//  AlertPresenter.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

class AlertPresenter {
        
    weak var delegate: AlertPresentationDelegate?
    
    lazy var alertController: UIAlertController = {
        
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let retry = UIAlertAction(title: "Retry", style: .default) { [weak self] (_) in
            guard let presenter = self else { return }
            
            presenter.delegate?.userDidTapRetryButton()
        }
        
        alert.addAction(retry)
        
        return alert
    }()
    
    let message: String
    
    init(message: String) {
        self.message = message
    }
    
}
