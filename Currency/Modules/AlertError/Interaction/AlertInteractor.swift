//
//  AlertInteractor.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import Foundation

class AlertInteractor {
    
    let show: Transition
    let alertPresenter: AlertPresenter
    
    weak var delegate: AlertErrorInteractionDelegate?
    
    init(show: Transition,
         alertPresenter: AlertPresenter) {
        self.show = show
        self.alertPresenter = alertPresenter
    }
}

extension AlertInteractor: Interactable {
    
    func start() {
        show.perform()
    }
    
}

extension AlertInteractor: AlertPresentationDelegate {
    
    func userDidTapRetryButton() {
        delegate?.userDidTapRetry(self)
    }
    
}
