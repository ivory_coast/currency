//
//  AlertInteraction.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import Foundation

protocol AlertErrorInteractionFactory {
    
    func createAlertInteraction(error: Error, delegate: AlertErrorInteractionDelegate) -> Interactable
    
}

protocol AlertErrorInteractionDelegate: class {
    
    func userDidTapRetry(_: Interactable)
    
}
