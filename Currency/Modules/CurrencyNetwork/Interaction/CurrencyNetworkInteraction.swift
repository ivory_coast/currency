//
//  CurrencyNetworkInteraction.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import Foundation

protocol CurrencyNetworkInteractionFactory {
    
    func createCurrencyNetworkInteraction(delegate: CurrencyNetworkInteractionDelegate) -> Interactable
    
}

protocol CurrencyNetworkInteractionDelegate: class {
    
    func currencyNetworkInteraction(_: Interactable, didSuccess currencies: [String: Float])
    func currencyNetworkInteraction(_: Interactable, didFailure error: Error)
    
}
