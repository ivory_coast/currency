//
//  CurrencyNetworkInteractor.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import Foundation

class CurrencyNetworkInteractor {
    
    let networkService: ICurrencyLoader
    let presentation: ActivityPresentation
    let show: Transition
    let dismiss: Transition
    
    weak var delegate: CurrencyNetworkInteractionDelegate?
    
    init(networkService: ICurrencyLoader,
         presentation: ActivityPresentation,
         show: Transition,
         dismiss: Transition) {
        self.networkService = networkService
        self.presentation = presentation
        self.show = show
        self.dismiss = dismiss
    }
    
    private var response: CurrencyResponse?
}

extension CurrencyNetworkInteractor: Interactable {
    
    func start() {
        show.perform()
        networkService.loadCurrencies { [weak self] (response) in

            guard let interactior = self else { return }

            interactior.response = response
            interactior.dismiss.perform()
        }
    }
}

extension CurrencyNetworkInteractor: TransitionDelegate {
    
    func didFinish(_ transition: Transition) {
        
        guard let response = response, transition === dismiss else { return }
        
        if let currencies = response.currencies  {
            delegate?.currencyNetworkInteraction(self, didSuccess: currencies)

        } else {
            let error = response.error ?? NetworkError.networkError
            delegate?.currencyNetworkInteraction(self, didFailure: error)
        }
        
    }
    
}
