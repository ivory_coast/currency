//
//  RootNavigationInteraction.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import Foundation

protocol RootNavigationInteractionFactory {
    
    func createRootNavigationInteraction() -> Interactable
    
}
