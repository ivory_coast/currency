//
//  RootNavigationInteractor.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import Foundation

class RootNavigationInteractor {
    
    let presentation: RootNavigationPresentation
    let show: Transition
    
    init(presentation: RootNavigationPresentation, show: Transition) {
        self.presentation = presentation
        self.show = show
    }
    
}

extension RootNavigationInteractor: Interactable {
    
    func start() {
        show.perform()
    }
    
}
