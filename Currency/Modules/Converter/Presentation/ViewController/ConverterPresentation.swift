//
//  ConverterPresentation.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

protocol ConverterPresentation: class {
 
    func didBeginEditing()
    func didFinishCurrenciesReloading()

}

protocol ConverterPresentationDelegate: class {
    
    var reloadCurrenciesTimeoutInSeconds: Int { get }
    
    func didFinishEditing()
    func reloadCurrencies()
    
}
