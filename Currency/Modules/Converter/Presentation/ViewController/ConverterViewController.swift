//
//  ConverterViewController.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 18/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

class ConverterViewController: UIViewController {
    
    var tableView: UITableView!
    var doneBarButtonItem: UIBarButtonItem!
    
    weak var delegate: ConverterPresentationDelegate?
    
    private let keyboardFrameObserver = KeyboardFrameObserver()
    
    // MARK: - Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        keyboardFrameObserver.start { [weak self] change in
            guard let viewController = self else { return }
            
            let keyboardFrameInView = viewController.view.convert(change.frame, from: nil)
            let intersection = viewController.view.safeAreaLayoutGuide.layoutFrame.maxY - keyboardFrameInView.minY
            
            viewController.tableView.contentInset.bottom = intersection
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        keyboardFrameObserver.stop()
    }
    
    // MARK: - Actions
    
    @objc
    func didFinishEditing(_ sender: UIBarButtonItem) {
        view.endEditing(true)
        navigationItem.rightBarButtonItems = []
        delegate?.didFinishEditing()
    }
    
}

extension ConverterViewController: ConverterPresentation {
    
    func didBeginEditing() {
        navigationItem.rightBarButtonItems = [doneBarButtonItem]
    }
    
    func didFinishCurrenciesReloading() {
        guard let timeout = delegate?.reloadCurrenciesTimeoutInSeconds else { return }
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(timeout)){
            self.delegate?.reloadCurrencies()
        }
    }
    
}
