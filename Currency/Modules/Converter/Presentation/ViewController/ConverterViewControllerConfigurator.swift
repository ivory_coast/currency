//
//  ConverterViewControllerConfigurator.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 24/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

class ConverterViewControllerConfigurator {
    
    let builder: ViewBuilder
    
    init(builder: ViewBuilder) {
        self.builder = builder
    }
    
    func configure(viewController: ConverterViewController) {
        
        viewController.tableView = buildTableView(addedTo: viewController.view)
        viewController.tableView.register(CurrencyCell.self, forCellReuseIdentifier: "\(CurrencyCell.self)")
        viewController.tableView.preservesSuperviewLayoutMargins = true
        
        viewController.doneBarButtonItem =  UIBarButtonItem(
                barButtonSystemItem: .done,
                target: viewController,
                action: #selector(viewController.didFinishEditing(_:)))
    }
    
}

private extension ConverterViewControllerConfigurator {
 
    func buildTableView(addedTo superview: UIView) -> UITableView {
        let t: UITableView = builder.build(addedTo: superview)
        let guide = superview.safeAreaLayoutGuide
        t.topAnchor.constraint(equalTo: guide.topAnchor).isActive = true
        t.bottomAnchor.constraint(equalTo: guide.bottomAnchor).isActive = true
        t.leadingAnchor.constraint(equalTo: guide.leadingAnchor).isActive = true
        t.trailingAnchor.constraint(equalTo: guide.trailingAnchor).isActive = true
        
        return t
    }

}
