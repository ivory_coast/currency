//
//  CurrencyCellViewModel.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 25/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import Foundation

struct CurrencyCellViewModel {
    let currencyNameText: String
    let relativeAmountText: String
}
