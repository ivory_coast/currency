//
//  CurrencyCellConfigurator.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 24/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

class CurrencyCellConfigurator {
    
    let builder: ViewBuilder
    
    init(builder: ViewBuilder) {
        self.builder = builder
    }
    
    func configure(cell: CurrencyCell) {
        
        cell.preservesSuperviewLayoutMargins = true
        cell.contentView.preservesSuperviewLayoutMargins = true
        
        cell.nameLabel = buildNameLabel(addedTo: cell.contentView)
        cell.amountLabel = buildAmountLabel(addedTo: cell.contentView)
        cell.inputTextField = buildInputTextField(addedTo: cell.contentView)
        cell.contentView.bringSubviewToFront(cell.amountLabel)
        
        cell.selectionStyle = .none
        cell.backgroundColor = .white
        cell.contentView.backgroundColor = .white
    }
}

private extension CurrencyCellConfigurator {
    
    func buildNameLabel(addedTo superview: UIView) -> UILabel {
        let n: UILabel = builder.build(addedTo: superview)
        n.font = UIFont.systemFont(ofSize: 19)
        n.numberOfLines = 0
        n.textAlignment = .left
        n.backgroundColor = .white
        
        return n
    }
    
    func buildAmountLabel(addedTo superview: UIView) -> UILabel {
        let a: UILabel = builder.build(addedTo: superview)
        a.font = UIFont.systemFont(ofSize: 19)
        a.numberOfLines = 0
        a.textAlignment = .right
        a.backgroundColor = .white
        
        return a
    }
    
    func buildInputTextField(addedTo superview: UIView) -> UITextField {
        let i: UITextField = builder.build(addedTo: superview)
        i.font = UIFont.systemFont(ofSize: 19)
        i.textAlignment = .right
        i.placeholder = "0"
        i.keyboardType = .decimalPad
        i.backgroundColor = .white
        
        return i
    }

}
