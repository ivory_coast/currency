//
//  CurrencyCellLayoutCalculator.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 24/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

class CurrencyCellLayoutCalculator {
    
    let nameLabelWidth: CGFloat
    
    init(nameLabelWidth: CGFloat) {
        self.nameLabelWidth = nameLabelWidth
    }
    
    func calculateLayout(cell: CurrencyCell) {
        cell.nameLabel.frame = calculateNameLabelFrame(superview: cell.contentView)
        cell.amountLabel.frame = calculateAmountLabelFrame(superview: cell.contentView)
        cell.inputTextField.frame = calculateInputTextFieldFrame(superview: cell.contentView)
    }
}

private extension CurrencyCellLayoutCalculator {
    
    func calculateNameLabelFrame(superview: UIView) -> CGRect {
        return CGRect(
            x: superview.layoutMargins.left,
            y: 0,
            width: nameLabelWidth,
            height: superview.bounds.height)
    }
    
    func calculateAmountLabelFrame(superview: UIView) -> CGRect {
        return CGRect(
            x: (superview.layoutMargins.left + nameLabelWidth),
            y: 0,
            width: superview.bounds.width - (superview.layoutMargins.left + nameLabelWidth + superview.layoutMargins.right),
            height: superview.bounds.height)
    }
    
    func calculateInputTextFieldFrame(superview: UIView) -> CGRect {
        return calculateAmountLabelFrame(superview: superview)
    }
    
}
