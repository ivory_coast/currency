//
//  CurrencyCell.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 18/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

class CurrencyCell: UITableViewCell {
    
    var nameLabel: UILabel!
    var amountLabel: UILabel!
    var inputTextField: UITextField!

    // MARK: - Memory Management
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        CurrencyCellConfigurator(builder: ViewBuilder()).configure(cell: self)
        DispatchQueue.main.async {
            CurrencyCellLayoutCalculator(nameLabelWidth: 100).calculateLayout(cell: self)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        inputTextField.isEnabled = selected
        let topSubview: UIView! = selected ? inputTextField : amountLabel
        contentView.bringSubviewToFront(topSubview)
        
        guard selected else { return }
        
        inputTextField.text = amountLabel.text
        inputTextField.becomeFirstResponder()
    }
    
    // MARK: - Configuration
    
    func configure(_ viewModel: CurrencyCellViewModel) {
        nameLabel.text = viewModel.currencyNameText
        amountLabel.text = viewModel.relativeAmountText
    }
    
}
