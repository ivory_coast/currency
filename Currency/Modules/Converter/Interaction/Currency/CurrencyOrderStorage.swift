//
//  CurrencyOrderStorage.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 27/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import Foundation

class CurrencyOrderStorage {
    
    private enum Key {
        static let currencyOrder = "currencyOrder"
    }
    
    private let defaults: UserDefaults = .standard
    
    var currencyOrder: [String]? {
        get {
            let value = defaults.value(forKey: Key.currencyOrder) as? [String]
            return value
        }
        set {
            defaults.set(newValue, forKey: Key.currencyOrder)
        }
    }
    
    func clear() {
        defaults.removeObject(forKey: Key.currencyOrder)
    }
}
