//
//  BaseCurrencyStorage.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 26/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import Foundation

protocol IBaseCurrencyStorage: class {
    
    var baseCurrency: String { get }
    var baseAmount: Float { get set }
    
    func change(baseCurrency: String, baseCurrencyRate: Float)
    func clear()
}

class BaseCurrencyStorage: IBaseCurrencyStorage {
    
    private enum Key {
        static let baseCurrency = "baseCurrency"
        static let baseAmount = "baseAmount"
    }
    
    private let defaults: UserDefaults = .standard
    
    private let defaultBaseCurrency: String
    private let defaultBaseAmount: Float
    
    init(defaultBaseCurrency: String,
         defaultBaseAmount: Float) {
        self.defaultBaseCurrency = defaultBaseCurrency
        self.defaultBaseAmount = defaultBaseAmount
    }
    
    private(set) var baseCurrency: String {
        get {
            let value = defaults.value(forKey: Key.baseCurrency) as? String
            return value ?? defaultBaseCurrency
        }
        set {
            defaults.set(newValue, forKey: Key.baseCurrency)
        }
    }
    
    var baseAmount: Float {
        get {
            let value = defaults.value(forKey: Key.baseAmount) as? Float
            return value ?? defaultBaseAmount
        }
        set {
            defaults.set(newValue, forKey: Key.baseAmount)
        }
    }
    
    func change(baseCurrency: String, baseCurrencyRate: Float) {
        self.baseCurrency = baseCurrency
        baseAmount = baseAmount / baseCurrencyRate
    }
    
    func clear() {
        defaults.removeObject(forKey: Key.baseAmount)
        defaults.removeObject(forKey: Key.baseCurrency)
    }
    
}
