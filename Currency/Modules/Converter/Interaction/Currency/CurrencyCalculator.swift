//
//  CurrencyCalculator.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 22/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import Foundation

class CurrencyCalculator {
    
    private let storage: IBaseCurrencyStorage
    
    init(storage: IBaseCurrencyStorage) {
        self.storage = storage
    }
    
    var baseAmount: Float {
        get {
            return storage.baseAmount
        }
        set {
           storage.baseAmount = newValue
        }
    }
    
    func didChange(relativeAmount: Float, forCurrencyRate currencyRate: Float) {
        baseAmount = relativeAmount / currencyRate
    }
    
    func calculateRelativeAmount(forCurrencyRate currencyRate: Float) -> Float {
        return baseAmount * currencyRate
    }
    
}
