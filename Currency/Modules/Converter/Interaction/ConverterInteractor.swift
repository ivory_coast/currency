//
//  ConverterInteractor.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import Foundation

class ConverterInteractor {
    
    let reloadCurrenciesTimeoutInSeconds: Int
    let networkService: ICurrencyLoader
    let presentation: ConverterPresentation
    let tableFlow: CurrencyTableFlow
    let show: Transition
    
    init(reloadCurrenciesTimeoutInSeconds: Int,
         networkService: ICurrencyLoader,
         presentation: ConverterPresentation,
         tableFlow: CurrencyTableFlow,
         show: Transition) {
        self.reloadCurrenciesTimeoutInSeconds = reloadCurrenciesTimeoutInSeconds
        self.networkService = networkService
        self.presentation = presentation
        self.tableFlow = tableFlow
        self.show = show
    }
    
}

extension ConverterInteractor: Interactable {
    
    func start() {
        show.perform()
    }
}

extension ConverterInteractor: TransitionDelegate {
    
    func didFinish(_ transition: Transition) {
        guard transition === show else { return }
        reloadCurrencies()
    }
}

extension ConverterInteractor: ConverterPresentationDelegate {
    
    func didFinishEditing() {
        tableFlow.didFinishEditing()
    }
    
    func reloadCurrencies() {
        networkService.loadCurrencies { [weak self] response in
            guard let interactor = self else { return }
            
            if let currencies = response.currencies {
                interactor.tableFlow.update(currencies: currencies)
            }
            
            interactor.presentation.didFinishCurrenciesReloading()
        }
    }
    
}

extension ConverterInteractor:  CurrencyTableFlowDelegate {
    
    func didBeginEditing() {
        presentation.didBeginEditing()
    }
    
}
