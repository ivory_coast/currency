//
//  CurrencyTableFlow.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

protocol CurrencyTableFlowDelegate: class {
    func didBeginEditing()
}

class CurrencyTableFlow: NSObject, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    weak var tableView: UITableView?
    weak var delegate: CurrencyTableFlowDelegate?

    // MARK: - Memory Management
    
    private let tableData: CurrencyTableData
    private let formatter: CurrencyFormatter
    private let validator: CurrencyAmountValidator

    init(tableData: CurrencyTableData,
         formatter: CurrencyFormatter,
         validator: CurrencyAmountValidator) {
        self.tableData = tableData
        self.formatter = formatter
        self.validator = validator
        super.init()
    }
    
    // MARK: - UITableViewDataSource, UITableViewDelegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: "\(CurrencyCell.self)", for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? CurrencyCell else { return }
        cell.inputTextField.delegate = self
        cell.inputTextField.addTarget(self, action: #selector(updateTopRowRelativeAmount), for: .editingChanged)
        cell.configure(tableData.getViewModel(at: indexPath))
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didBeginEditing()
        tableView.beginUpdates()
        tableView.moveRow(at: indexPath, to: .zero)
        tableData.moveRow(at: indexPath, to: .zero)
        tableView.endUpdates()
        tableView.scrollToRow(at: .zero, at: .top, animated: true)
    }
    
    // MARK: - UITextFieldDelegate
    
    @objc
    func updateTopRowRelativeAmount(_ sender: UITextField) {
        sender.text? = validator.finalise(currencyAmountString: sender.text)
        let relativeAmount = formatter.number(from: sender.text ?? "0") ?? 0
        updateBaseAmount(relativeAmount.floatValue, at: .zero)
        updateVisibleCells()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        tableView?.scrollToRow(at: .zero, at: .top, animated: true)
        return validator.validate(currencyAmountString: textField.text, shouldChangeCharactersIn: range, replacementString: string)
    }
    
    // MARK: - Updating data
    
    func update(currencies: [String: Float]) {
        tableData.update(currencies: currencies)
        updateVisibleCells()
    }
    
    func didFinishEditing() {
        tableView?.deselectRow(at: .zero, animated: false)
    }
    
}

private extension CurrencyTableFlow {
    
    func updateVisibleCells() {
        guard
            let tableView = tableView,
            let cellsToUpdate = Array(tableView.visibleCells) as? [CurrencyCell] else { return }
            let cellsIndexPaths = cellsToUpdate.compactMap { tableView.indexPath(for: $0) }
        
        zip(cellsToUpdate, cellsIndexPaths).forEach { (cell, indexPath) in
            cell.configure(tableData.getViewModel(at: indexPath))
        }
    }
    
    func updateBaseAmount(_ relativeAmount: Float, at indexPath: IndexPath) {
        tableData.updateBaseAmount(from: relativeAmount, at: indexPath)
    }
    
}
