//
//  CurrencyTableData.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import Foundation

class CurrencyTableData {
    
    private var currencies: OrderedDictionary<String, Float> = []
    private let calculator: CurrencyCalculator
    private let formatter: CurrencyFormatter
    private let orderStorage: CurrencyOrderStorage
    
    init(orderStorage: CurrencyOrderStorage,
         calculator: CurrencyCalculator,
         formatter: CurrencyFormatter,
         currencies: [String: Float]) {
        self.orderStorage = orderStorage
        self.calculator = calculator
        self.formatter = formatter
        self.initiateCurrencies(with: currencies)
    }
    
    func numberOfRows() -> Int {
        return currencies.count
    }
    
    func moveRow(at indexPath: IndexPath, to newIndexPath: IndexPath) {
        if let currency = currencies.remove(at: indexPath.row) {
            currencies.insert(currency, at: newIndexPath.row)
            storeCurrencyOrder()
        }
    }
    
    func updateBaseAmount(from relativeAmount: Float, at indexPath: IndexPath) {
        calculator.didChange(relativeAmount: relativeAmount, forCurrencyRate: currencies[indexPath.row].value)
    }
    
    func update(currencies: [String: Float]) {
        currencies.forEach{ (currency) in
            self.currencies[currency.key] = currency.value
        }
    }
    
    func getViewModel(at indexPath: IndexPath) -> CurrencyCellViewModel {
        return CurrencyCellViewModel(
            currencyNameText: getCurrencyName(at: indexPath),
            relativeAmountText: formatter.string(from: NSNumber(value: getRelativeAmount(at: indexPath))) ?? "0")
    }
}

private extension CurrencyTableData {
    
    func storeCurrencyOrder() {
        orderStorage.currencyOrder = currencies.orderedKeys.map{ String($0) }
    }
    
    func getCurrencyName(at indexPath: IndexPath) -> String {
        return currencies[indexPath.row].key
    }
    
    func getRelativeAmount(at indexPath: IndexPath) -> Float {
        return calculator.calculateRelativeAmount(forCurrencyRate: currencies[indexPath.row].value)
    }
    
    func initiateCurrencies(with currencies: [String: Float]) {
        
        if let order = orderStorage.currencyOrder {
            
            order.forEach { currencyName in
                let currencyRate = currencies[currencyName]
                self.currencies[currencyName] = currencyRate
            }
            
        } else {
            
            update(currencies: currencies)
            storeCurrencyOrder()
        }
    }

}
