//
//  AppCoordinator.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import Foundation

class AppCoordinator: Interactable {
    
    let rootNavigationInteractionFactory: RootNavigationInteractionFactory
    var rootNavigationInteraction: Interactable?
    
    let currencyCoordinatorFactory: ConverterCoordinatorFactory
    var currencyCoordinator: Interactable?
    
    init(rootNavigationInteractionFactory: RootNavigationInteractionFactory,
         currencyCoordinatorFactory: ConverterCoordinatorFactory) {
        self.rootNavigationInteractionFactory = rootNavigationInteractionFactory
        self.currencyCoordinatorFactory = currencyCoordinatorFactory

    }
    
    func start() {
        rootNavigationInteraction = rootNavigationInteractionFactory.createRootNavigationInteraction()
        rootNavigationInteraction?.start()
        
        currencyCoordinator = currencyCoordinatorFactory.createConverterCoordinator()
        currencyCoordinator?.start()
    }
}
