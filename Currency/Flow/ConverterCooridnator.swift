//
//  CurrencyCooridnator.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import Foundation

protocol ConverterCoordinatorFactory {
    
    func createConverterCoordinator() -> Interactable
}

class ConverterCoordinator {
    
    let converterInteractionFactory: ConverterInteractionFactory
    var converterInteraction: Interactable?
    
    let currencyNetworkInteractionFactory: CurrencyNetworkInteractionFactory
    var currencyNetworkInteraction: Interactable?
    
    let currencyErrorInteractionFactory: AlertErrorInteractionFactory
    var currencyErrorInteraction: Interactable?
    
    init(converterInteractionFactory: ConverterInteractionFactory,
         currencyNetworkInteractionFactory: CurrencyNetworkInteractionFactory,
         currencyErrorInteractionFactory: AlertErrorInteractionFactory) {
        self.converterInteractionFactory = converterInteractionFactory
        self.currencyNetworkInteractionFactory = currencyNetworkInteractionFactory
        self.currencyErrorInteractionFactory = currencyErrorInteractionFactory
    }
    
}

extension ConverterCoordinator: Interactable {
    
    func start() {
        currencyNetworkInteraction = currencyNetworkInteractionFactory.createCurrencyNetworkInteraction(delegate: self)
        currencyNetworkInteraction?.start()
    }
    
}

extension ConverterCoordinator: CurrencyNetworkInteractionDelegate {
    
    func currencyNetworkInteraction(_: Interactable, didSuccess currensies: [String : Float]) {
        converterInteraction = converterInteractionFactory.createConverterInteraction(currencies: currensies)
        converterInteraction?.start()
    }
    
    func currencyNetworkInteraction(_: Interactable, didFailure error: Error) {
        currencyErrorInteraction = currencyErrorInteractionFactory.createAlertInteraction(error: error, delegate: self)
        currencyErrorInteraction?.start()
    }
    
}

extension ConverterCoordinator: AlertErrorInteractionDelegate {
    
    func userDidTapRetry(_: Interactable) {
        currencyNetworkInteraction?.start()
    }
    
}
