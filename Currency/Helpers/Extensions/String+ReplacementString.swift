//
//  String+ReplacementString.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 26/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import Foundation

extension String {
    
    func replacing(nsRange: NSRange, replacementString: String) -> String {
        let nsString = self as NSString
        let newString = nsString.replacingCharacters(in: nsRange, with: replacementString)
        
        return newString
    }
    
}
