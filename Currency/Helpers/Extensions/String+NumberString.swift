//
//  String+NumberString.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 26/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import Foundation

extension String {
    
    var isNumberString: Bool {
        return rangeOfCharacter(from: CharacterSet.decimalDigits
            .union(CharacterSet(charactersIn: Locale.separatorDecimal))
            .inverted) == nil
    }
    
    mutating func trimFirstInsignificantZeros() {
        guard isNumberString else { return }
        
        while hasPrefix("0") && !hasPrefix("0" + Locale.separatorDecimal) && count > 1 {
            self = String(dropFirst())
        }
    }
    
    mutating func trimLastInsignificantZeros() {
        guard isNumberString else { return }
        
        while (hasSuffix("0") || hasSuffix(Locale.separatorDecimal)) && count > 1 {
            self = String(dropLast())
        }
    }
    
    mutating func addEmptyIntegerPartIfNeeded() {
        if self == Locale.separatorDecimal {
            self = "0" + Locale.separatorDecimal
        }
    }
}
