//
//  KeyboardFrameObserver.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 26/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

class KeyboardFrameObserver {
    
    struct KeyboardFrameChange {
        let frame: CGRect
        let animationDuration: TimeInterval
        let animationCurve: UIView.AnimationOptions
    }
    
    private var observer: NSObjectProtocol?
    
    func start(with action: @escaping (_ change: KeyboardFrameChange) -> ()) {
        guard observer == nil else { return }
        
        observer = NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillChangeFrameNotification, object: nil, queue: OperationQueue.main) { (notification) in
            guard
                let userInfo = notification.userInfo,
                let frame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
                else { return }
            
            let animationDuration: TimeInterval = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawValue = (notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve = UIView.AnimationOptions(rawValue: animationCurveRawValue)
            
            action(KeyboardFrameChange(frame: frame, animationDuration: animationDuration, animationCurve: animationCurve))
        }
    }
    
    func stop() {
        guard let observer = observer else { return }
        
        NotificationCenter.default.removeObserver(observer, name: nil, object: nil)
        
        self.observer = nil
    }
    
}
