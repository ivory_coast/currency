//
//  ViewBuilder.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 24/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

class ViewBuilder { 
    
    func build<View: UIView>(addedTo superview: UIView) -> View {
        let view = View.init()
        superview.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }
}
