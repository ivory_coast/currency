//
//  DismissTransition.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

class DismissTransition: Transition {
    
    weak var delegate: TransitionDelegate?
    
    let presenterViewController: UIViewController
    
    init(presenterViewController: UIViewController) {
        self.presenterViewController = presenterViewController
    }
    
    func perform() {
        presenterViewController.dismiss(animated: true) { [weak self] in
            guard let transition = self else { return }
            transition.delegate?.didFinish(transition)
        }
    }
    
}
