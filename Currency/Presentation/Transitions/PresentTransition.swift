//
//  PresentTransition.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

class PresentTransition: Transition {
    
    let presenterViewController: UIViewController
    let presentingViewController: UIViewController
    
    init(presenterViewController: UIViewController, presentingViewController: UIViewController) {
        self.presenterViewController = presenterViewController
        self.presentingViewController = presentingViewController
    }
    
    func perform() {
        presenterViewController.present(presentingViewController, animated: true, completion: nil)
    }
    
}
