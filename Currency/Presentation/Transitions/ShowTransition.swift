//
//  ShowTransition.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 21/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

class ShowTransition: Transition {
    
    weak var delegate: TransitionDelegate?
    
    let presenterViewController: UINavigationController
    let presentingViewController: UIViewController
    
    init(presenterViewController: UINavigationController, presentingViewController: UIViewController) {
        self.presenterViewController = presenterViewController
        self.presentingViewController = presentingViewController
    }
    
    func perform() {
        CATransaction.begin()
        CATransaction.setCompletionBlock { [weak self] in
            guard let transition = self else { return }
            transition.delegate?.didFinish(transition)
        }
        presenterViewController.show(presentingViewController, sender: nil)
        CATransaction.commit()
    }
    
}
