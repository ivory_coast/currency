//
//  WindowTransition.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

class WindowTransition: Transition {
    
    let viewController: UIViewController
    let window: UIWindow
    
    init(viewController: UIViewController, window: UIWindow) {
        self.viewController = viewController
        self.window = window
    }
    
    func perform() {
        window.rootViewController = viewController
        
        window.makeKeyAndVisible()
    }
    
}
