//
//  CurrencyFormatter.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import Foundation

class CurrencyFormatter: NumberFormatter {
 
    override init() {
        super.init()
        
        numberStyle = .decimal
        minimumFractionDigits = 0
        maximumFractionDigits = 2
        usesGroupingSeparator = false
        decimalSeparator = Locale.separatorDecimal
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
