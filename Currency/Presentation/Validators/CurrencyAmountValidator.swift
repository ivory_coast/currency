//
//  CurrencyAmountValidator.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 26/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import Foundation

class CurrencyAmountValidator {
    
    let maximumCurrencyAmountStringLength: Int
    
    init(maximumCurrencyAmountStringLength: Int) {
        self.maximumCurrencyAmountStringLength = maximumCurrencyAmountStringLength
    }
    
    func finalise(currencyAmountString: String?) -> String {
        guard var text = currencyAmountString else { return "" }
        
        text.trimFirstInsignificantZeros()
        text.addEmptyIntegerPartIfNeeded()
        
        return text
    }
    
    func validate(currencyAmountString: String?, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = currencyAmountString  else { return false }
        
        let isDoubleSeparator: Bool = {
            return string == Locale.separatorDecimal && text.contains(Locale.separatorDecimal)
        }()
        
        let isDoubleInsignificantZero: Bool = {
            return string == "0" && text.hasPrefix("0") && !text.contains(Locale.separatorDecimal)
        }()
        
        let isOutOfLength: Bool = {
            let textAfterUpdate = text.replacing(nsRange: range, replacementString: string)
            return textAfterUpdate.count > maximumCurrencyAmountStringLength
        }()
        
        return !isDoubleSeparator && !isDoubleInsignificantZero && !isOutOfLength
    }
    
}
