//
//  CurrencyModel.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 23/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import Foundation

struct Container {
    let currencies: [String: Float]
}

extension Container: Decodable {
    
    enum RootCodingKeys: CodingKey {
        case base
        case rates
    }
    
    init(from decoder: Decoder) throws {
        let rootContainer = try decoder.container(keyedBy: RootCodingKeys.self)
        let base = try rootContainer.decode(String.self, forKey: .base)
        var rates = try rootContainer.decode([String : Float].self, forKey: .rates)
        
        rates[base] = 1
        
        currencies = rates
    }
}
