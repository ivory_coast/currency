//
//  ConverterCoordinatorBuilder.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

class ConverterCoordinatorBuilder: ConverterCoordinatorFactory {
    
    private let context: Context
    
    init(context: Context) {
        self.context = context
    }
    
    func createConverterCoordinator() -> Interactable {
        
        return ConverterCoordinator(
            converterInteractionFactory: ConverterInteractorBuilder(context: context),
            currencyNetworkInteractionFactory: CurrencyNetworkInteractorBuilder(context: context),
            currencyErrorInteractionFactory: AlertErrorInteractorBuilder(context: context))
    }
    
}
