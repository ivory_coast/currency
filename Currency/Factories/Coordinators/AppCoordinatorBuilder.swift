//
//  AppCoordinatorBuilder.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import Foundation

class AppCoordinatorBuilder {
    
    func createAppCoordinator() -> AppCoordinator {
        
        let currencyStorage = BaseCurrencyStorage(defaultBaseCurrency: "EUR", defaultBaseAmount: 1)
        let navigationController = RootNavigationControllerFactory().create()
        
        let context = Context(
            currencyStorage: currencyStorage,
            currencyFormatter: CurrencyFormatter(),
            currencyValidator: CurrencyAmountValidator(maximumCurrencyAmountStringLength: 10),
            navigationController: navigationController,
            currencyNetworkService: CurrencyNetworkService(baseUrl: "https://revolut.duckdns.org",
                                                           baseCurrency: currencyStorage.baseCurrency),
            reloadCurrenciesTimeoutInSeconds: 1)

        return AppCoordinator(
            rootNavigationInteractionFactory: RootNavigationInteractorBuilder(navigationController: navigationController),
            currencyCoordinatorFactory: ConverterCoordinatorBuilder(context: context))
    }
    
}
