//
//  CurrencyNetworkInteractorBuilder.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

class CurrencyNetworkInteractorBuilder: CurrencyNetworkInteractionFactory {
    
    let context: Context
    
    init(context: Context) {
        self.context = context
    }
    
    func createCurrencyNetworkInteraction(delegate: CurrencyNetworkInteractionDelegate) -> Interactable {
        
        let presentingViewController = ActivityViewControllerFactory().create()
        
        let dismiss = DismissTransition(presenterViewController: context.navigationController)
        
        let interactor = CurrencyNetworkInteractor(
            networkService: context.currencyNetworkService,
            presentation: presentingViewController,
            show: PresentTransition(
                presenterViewController: context.navigationController,
                presentingViewController: presentingViewController),
            dismiss: dismiss)
        
        dismiss.delegate = interactor
        interactor.delegate = delegate
        
        return interactor
    }
    
}
