//
//  ConverterInteractorBuilder.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 21/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

class ConverterInteractorBuilder: ConverterInteractionFactory {
    
    let context: Context
    
    init(context: Context) {
        self.context = context
    }
    
    func createConverterInteraction(currencies: [String: Float]) -> Interactable {
        
        let presentingViewController: ConverterViewController = {
            let viewController = ConverterViewControllerFactory().create()
            ConverterViewControllerConfigurator(builder: ViewBuilder())
                .configure(viewController: viewController)
            
            return viewController
        }()
        
        let tableFlow = CurrencyTableFlow(
            tableData: CurrencyTableData(
                orderStorage: CurrencyOrderStorage(),
                calculator: CurrencyCalculator(storage: context.currencyStorage),
                formatter:  context.currencyFormatter,
                currencies: currencies),
            formatter: context.currencyFormatter,
            validator: context.currencyValidator)
        
        let show = ShowTransition(
            presenterViewController: context.navigationController,
            presentingViewController: presentingViewController)
        
        let interactor = ConverterInteractor(
            reloadCurrenciesTimeoutInSeconds: context.reloadCurrenciesTimeoutInSeconds,
            networkService: context.currencyNetworkService,
            presentation: presentingViewController,
            tableFlow: tableFlow,
            show: show)
        
        presentingViewController.delegate = interactor
        show.delegate = interactor
        
        let tableView: UITableView = {
            return presentingViewController.tableView
        }()
        
        tableFlow.tableView = tableView
        tableView.dataSource = tableFlow
        tableView.delegate = tableFlow
        
        tableFlow.delegate = interactor
        
        return interactor
    }
}
