//
//  AlertErrorInteractorBuilder.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

class AlertErrorInteractorBuilder: AlertErrorInteractionFactory {
    
    let context: Context
    
    init(context: Context) {
        self.context = context
    }
    
    func createAlertInteraction(error: Error, delegate: AlertErrorInteractionDelegate) -> Interactable {
        
        let presenter = AlertPresenter(message: error.localizedDescription)
        
        let interactor = AlertInteractor(
            show: PresentTransition(presenterViewController: context.navigationController,
                                    presentingViewController: presenter.alertController),
            alertPresenter: presenter)
        
        interactor.delegate = delegate
        presenter.delegate = interactor
        
        return interactor
    }
    
}
