//
//  RootNavigationInteractorBuilder.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

class RootNavigationInteractorBuilder: RootNavigationInteractionFactory {
    
    private let navigationController: RootNavigationController
    
    init(navigationController: RootNavigationController) {
        self.navigationController = navigationController
    }
    
    func createRootNavigationInteraction() -> Interactable {
        
        return RootNavigationInteractor(
            presentation: navigationController,
            show: WindowTransition(viewController: navigationController,
                                   window: WindowFactory().create()))
    }
    
}
