//
//  StoryboardViewControllerFactory.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

class StoryboardViewControllerFactory<ViewController> {
    
    let storyboardName: String
    
    init(storyboardName: String) {
        self.storyboardName = storyboardName
    }
    
    func create() -> ViewController {
        return UIStoryboard(name: storyboardName, bundle: nil).instantiateInitialViewController() as! ViewController
    }
    
}
