//
//  RootNavigationControllerFactory.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

class RootNavigationControllerFactory: StoryboardViewControllerFactory<RootNavigationController> {
    
    init() {
        super.init(storyboardName: "RootNavigation")
    }
    
    override func create() -> RootNavigationController {
        let viewController = super.create()
        viewController.view.backgroundColor = UIColor.white
        
        return viewController
    }
    
}
