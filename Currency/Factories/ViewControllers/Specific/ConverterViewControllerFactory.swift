//
//  ConverterViewControllerFactory.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 21/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

class ConverterViewControllerFactory: StoryboardViewControllerFactory<ConverterViewController> {
    
    init() {
        super.init(storyboardName: "Converter")
    }
    
    override func create() -> ConverterViewController {
        let viewController = super.create()
        viewController.navigationItem.hidesBackButton = true
        
        return viewController
    }
    
}
