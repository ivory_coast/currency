//
//  ActivityViewControllerFactory.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

class ActivityViewControllerFactory: StoryboardViewControllerFactory<ActivityViewController> {
    
    init() {
        super.init(storyboardName: "Activity")
    }
    
    override func create() -> ActivityViewController {
        let viewController = super.create()
        viewController.navigationItem.hidesBackButton = true
        viewController.modalTransitionStyle = .crossDissolve
        viewController.modalPresentationStyle = .overFullScreen
        
        return viewController
    }
    
}
