//
//  Context.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 25/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

class Context {
    
    let currencyStorage: IBaseCurrencyStorage
    let currencyFormatter: CurrencyFormatter
    let currencyValidator: CurrencyAmountValidator
    let navigationController: UINavigationController
    let currencyNetworkService: ICurrencyLoader
    let reloadCurrenciesTimeoutInSeconds: Int
    
    init(currencyStorage: IBaseCurrencyStorage,
         currencyFormatter: CurrencyFormatter,
         currencyValidator: CurrencyAmountValidator,
         navigationController: UINavigationController,
         currencyNetworkService: ICurrencyLoader,
         reloadCurrenciesTimeoutInSeconds: Int) {
        self.currencyStorage = currencyStorage
        self.currencyFormatter = currencyFormatter
        self.currencyValidator = currencyValidator
        self.navigationController = navigationController
        self.currencyNetworkService = currencyNetworkService
        self.reloadCurrenciesTimeoutInSeconds = reloadCurrenciesTimeoutInSeconds
    }
}
