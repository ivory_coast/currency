//
//  CurrencyNetworkService.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 20/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import Foundation

struct CurrencyResponse {
    let currencies: [String: Float]?
    let error: Error?
}

protocol ICurrencyLoader {
    func loadCurrencies(completion: @escaping (CurrencyResponse) -> Void) 
}

enum NetworkError: Error {
    case requestTimeoutError
    case requestError
    case authorizationError
    case unknownError
    case emptyResponseError
    case serializationError
    case serverError(message: String, code: Int)
    case networkError
}

class CurrencyNetworkService: ICurrencyLoader {
    
    let session = URLSession(configuration: .default)
    
    let baseUrl: String
    let baseCurrency: String
    
    init(baseUrl: String,
         baseCurrency: String) {
        self.baseUrl = baseUrl
        self.baseCurrency = baseCurrency
    }

    func loadCurrencies( completion: @escaping (CurrencyResponse) -> Void) {

        let queryString = "base=\(baseCurrency)"
        let urlString = baseUrl + "/latest" + "?" + queryString
        
        guard let url = URL(string: urlString) else {
            DispatchQueue.main.async {
                completion(CurrencyResponse(currencies: nil, error: NetworkError.requestError))
            }
            return
        }

        session.dataTask(with: url) { (data, response, error) in
            guard
                let data = data,
                let container = try? JSONDecoder().decode(Container.self, from: data) else {
                    DispatchQueue.main.async {
                        completion(CurrencyResponse(currencies: nil, error: NetworkError.serializationError))
                    }
                    return
            }
            DispatchQueue.main.async {
                completion(CurrencyResponse(currencies: container.currencies, error: nil))
            }
            }.resume()

    }

}
