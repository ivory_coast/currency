//
//  AppDelegate.swift
//  Currency
//
//  Created by Oleg Kolomyitsev on 18/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var appCoordinator: AppCoordinator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        appCoordinator = AppCoordinatorBuilder().createAppCoordinator()
        appCoordinator?.start()

        return true
    }

}
