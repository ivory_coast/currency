# iOS Test Application

## Task 
You should implement one screen with a list of currencies

![picture](images/img001.png)

The app must download and update rates every 1 second using API
https://revolut.duckdns.org/latest?base=EUR

List all currencies you get from the endpoint (one per row). Each row has an input where you can enter any amount of money. When you tap on currency row it should slide to top and its input becomes first responder. When you’re changing the amount the app must simultaneously update the corresponding value for other currencies.

Use swift programming language and any libraries you want. UI does not have to be exactly the same, it’s up to you.

Unit tests must be included.

Don’t use Rx.

Video demo: https://youtu.be/omcS-6LeKoo

## Supported Platforms

- iOS

## Screenshots

![currency001](https://user-images.githubusercontent.com/27812408/51802467-92192300-225b-11e9-9bd4-383410cbbabb.gif)

![currency002](https://user-images.githubusercontent.com/27812408/51802480-b117b500-225b-11e9-9e64-9a64f244839e.gif)
