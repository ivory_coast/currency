//
//  CurrencyTableDataTests.swift
//  CurrencyTests
//
//  Created by Oleg Kolomyitsev on 22/01/2019.
//  Copyright © 2019 Oleg Kolomyitsev. All rights reserved.
//

import XCTest
@testable import Currency

class CurrencyCalculatorTests: XCTestCase {
    
    class BaseCurrncyStorageMock: IBaseCurrencyStorage {
        
        var baseCurrency: String = "EUR"
        var baseAmount: Float = 1
        
        func change(baseCurrency: String, baseCurrencyRate: Float) {
            self.baseCurrency = baseCurrency
            baseAmount = baseAmount / baseCurrencyRate
        }
        
        func clear() {
            baseCurrency = "EUR"
            baseAmount = 1
        }
        
    }
    
    var currencyCalculator = CurrencyCalculator(storage: BaseCurrncyStorageMock())
    
    let accuracy: Float = 0.00001
    
    var currencyRate: Float = 0
    var currencyRelativeAmount: Float = 0

    override func setUp() {
        
        currencyRate = randomValue
        currencyRelativeAmount = randomValue

        currencyCalculator.didChange(relativeAmount: currencyRelativeAmount, forCurrencyRate: currencyRate)
    }
    
    func testBaseAmountWhenDidChangeRelativeAmount() {

        // given
        randomArray.forEach { newRelativeAmount in
            
            // when
            currencyCalculator.didChange(relativeAmount: newRelativeAmount, forCurrencyRate: currencyRate)
            
            // then
            XCTAssertEqual(currencyCalculator.baseAmount, newRelativeAmount / currencyRate, accuracy: accuracy)
        }
    }
    
    func testBaseAmountWhenDidChangeCurrencyRate() {
        
        // given
        randomArray.forEach { newCurrencyRate in
            
            // when
            currencyCalculator.didChange(relativeAmount: currencyRelativeAmount, forCurrencyRate: newCurrencyRate)
            
            // then
            XCTAssertEqual(currencyCalculator.baseAmount, currencyRelativeAmount / newCurrencyRate, accuracy: accuracy)
        }
    }
    
    func testRelativeAmountWhenDidChangeCurrencyRate() {
        
        // given
        randomArray.forEach { newCurrencyRate in
            
            // when
            let newRelativeAmount = currencyCalculator.calculateRelativeAmount(forCurrencyRate: newCurrencyRate)
            
            // then
            XCTAssertEqual(newRelativeAmount, currencyCalculator.baseAmount * newCurrencyRate, accuracy: accuracy)
        }
    }
}

private extension CurrencyCalculatorTests {
    
    private var randomValue: Float {
        return Float.random(in: 0.0001 ..< 1000 )
    }
    
    private var randomArray: [Float] {
        return Array(0..<100).map { _ in return randomValue }
    }

}
